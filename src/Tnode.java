import java.util.*;

// https://www.geeksforgeeks.org/expression-tree/

public class Tnode{

   public static void main (String[] param) {
      String rpn = "1 2 +";
//      String rpn = "";
//      String rpn = null;
//      String rpn = "2 1 - 4 * 6 3 / +";
//      String rpn ="512 1 - 4 * -61 3 / +";
//      String rpn = "+";
//      String rpn = "1 2 3 4 5 + * - /";
//      String rpn = "4 8 9 + *";
//      String rpn =  "2 7 * 6 3 / +";
//      String rpn = "7 4 - 7 / 4 3 * 4 8 9 + * - * 10 1 1 + / 2 4 * - +";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);
   }

   private final String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   private static final ArrayList<String> ALL_OPERATORS = new ArrayList<>
           (Arrays.asList("+", "-", "*", "/", "SWAP", "ROT", "DUP"));
   private static final ArrayList<String> OPERATORS = new ArrayList<>
           (Arrays.asList("+", "-", "*", "/"));
   private static final ArrayList<String> UNARY_OPERATORS = new ArrayList<>
           (Arrays.asList("SWAP", "ROT"));
   private static final ArrayList<String> INCREASE_OPERATORS = new ArrayList<>
           (Arrays.asList("DUP"));

   public Tnode(String name) {
      this.name = name;
   }

   public String getName() {
      return name;
   }

   public static Tnode buildFromRPN (String pol) {
      Tnode root = null;
      ArrayList<String> arguments;

      try {
         arguments = fractionateAndCleanString(pol);
      } catch (NullPointerException e) {
         throw new RuntimeException("Given argument is null, invalid input!" + e);
      } catch (IllegalArgumentException e) {
         throw new RuntimeException("'" + pol + "' is empty string, can't build tree. Invalid input!" + e);
      } catch (RuntimeException e) {
         throw new RuntimeException("'" + pol + "' consists only whitespace characters!" + e);
      }


      try {
         int check = checkIfInputValid(arguments, pol);
         if (check > 1) {
            throw new RuntimeException(pol + " consists too many numbers!");
         } else if (check < 1) {
            throw new RuntimeException(pol + " consists too few numbers!");
         }
      } catch (NumberFormatException e) {
         throw new RuntimeException(pol + " consists illegal symbols!" + e);
      }

      Stack<Tnode> nodes = new Stack<>();

      if (arguments.size() == 1) {
         root = new Tnode(arguments.get(0));
         return root;

      }

      for (String argument: arguments) {

         if (argument.equals("DUP")) {
            Tnode node1 = nodes.pop();
            Tnode node2 = cloneNode(node1);
            nodes.push(node1);
            nodes.push(node2);
         }else if (argument.equals("SWAP")) {
            try {
               Tnode node1 = nodes.pop();
               Tnode node2 = nodes.pop();
               nodes.push(node1);
               nodes.push(node2);
            } catch (EmptyStackException e) {
               throw new RuntimeException("Cannot perform SWAP operation, not enough nodes in stack: " + pol);
            }
         }else if (argument.equals("ROT")) {
            try {
               Tnode node1 = nodes.pop();
               Tnode node2 = nodes.pop();
               Tnode node3 = nodes.pop();
               nodes.push(node2);
               nodes.push(node1);
               nodes.push(node3);
            } catch (EmptyStackException e) {
               throw new RuntimeException("Cannot perform ROT operation, not enough nodes in stack: " + pol);
            }
         } else if (!ALL_OPERATORS.contains(argument)) {
            Tnode node = new Tnode(argument);
            nodes.push(node);
         } else {
            Tnode node = new Tnode(argument);

            // Pop two top nodes
            // Store top
            Tnode node1 = nodes.pop();      // Remove top
            Tnode node2 = nodes.pop();

            //  make them children
            node2.setNextSibling(node1);
            node.setFirstChild(node2);

            // System.out.println(t1 + "" + t2);
            // Add this subexpression to stack
            nodes.push(node);
         }
      }

      return nodes.pop();
   }

   private void setFirstChild(Tnode node) {
      this.firstChild = node;
   }

   private Tnode getFirstChild() {
      return this.firstChild;
   }

   private void setNextSibling(Tnode node) {
      this.nextSibling = node;
   }

   private Tnode getNextSibling() {
      return this.nextSibling;
   }

   private boolean hasFirstChild() {
      return this.firstChild != null;
   }

   private boolean hasNextSibling() {
      return this.nextSibling != null;
   }

   private static int checkIfInputValid(ArrayList<String> args, String pol) {
      int numbers = 0;
      int operatorsCount = 0;

      for (String arg : args) {
         if (OPERATORS.contains(arg)) {
            operatorsCount++;
         } else if (INCREASE_OPERATORS.contains(arg)){
            numbers++;
         } else if (!UNARY_OPERATORS.contains(arg)){
            try {
               Integer.parseInt(arg);
               numbers++;
            } catch (NumberFormatException e) {
               throw new NumberFormatException(e.toString());
            }
         }
      }
      return numbers - operatorsCount;
   }

   private static ArrayList<String> fractionateAndCleanString(String pol) {
      ArrayList<String> arguments = new ArrayList<>();
      String[] args;

      try {
         args = pol.split("\\s+");
      } catch (NullPointerException e) {
         throw new NullPointerException();
      }

      if (pol.isEmpty()) {
         throw new IllegalArgumentException();
      }

      for (String arg : args) {
         if (arg.trim().length() > 0) {
            arguments.add(arg.trim());
         }
      }
      if (arguments.isEmpty()) {
         throw new RuntimeException();
      }

      return arguments;
   }

   private static Tnode cloneNode(Tnode node) {
      Tnode clone = new Tnode(node.name);
      if(node.hasFirstChild()) {
         clone.setFirstChild(cloneNode(node.getFirstChild()));
      }
      if(node.hasNextSibling()) {
         clone.setNextSibling(cloneNode(node.getNextSibling()));
      }
      return clone;
   }

   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      b.append(this.getName());

      if (hasFirstChild()) {
         Tnode node = this.getFirstChild();
         b.append("(");
         b.append(node.toString());

      }

      if (hasNextSibling()) {
         Tnode node = this.getNextSibling();
         b.append(",");
         b.append(node.toString());
         b.append(")");
      }
      return b.toString();
   }

}

